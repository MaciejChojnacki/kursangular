export class User {
  name;
  city;
  constructor(name: string, city: string) {
    this.name = name;
    this.city = city;
  }
}
