import {Component} from '@angular/core';
import {User} from "./model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = "KursAngular"
  user = new User("Maciej", "Warszawa");
  isDetailsShown = false;
  colors = ["red", "orange", "green"];

  alert(message: string) {
    alert(message);
  }

  toggleDetails() {
    this.isDetailsShown = !this.isDetailsShown
  }
}
