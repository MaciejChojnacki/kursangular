import { Component } from '@angular/core';
import {User} from "../model";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent {
  user: User = new User("Maciej", "Warszawa");
}
